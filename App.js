import React from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducers from './src/reducers';
import { Header } from './src/components/shared'
import LibraryList from './src/components/LibraryList'

export default class App extends React.Component {
  render() {
    const { ContainerStyle } = styles;
    return (
      <Provider store={createStore(reducers)}>
        <View style={ContainerStyle}>
          <Header headerText="Tech Stack Info" />
          <LibraryList/>
        </View>
      </Provider>
    );
  }
}

const styles = {
  ContainerStyle: {
    flex: 1
  }
};
