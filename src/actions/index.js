export const selectItem = (library) => {
  return {
    type: "select_library",
    payload: library
  };
};