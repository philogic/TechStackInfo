import React from 'react';
import {
  Text,
  TouchableWithoutFeedback,
  View,
  LayoutAnimation
} from 'react-native';
import {connect} from 'react-redux';
import {CardSection} from "./shared";
import * as actions from '../actions';

class ListItem extends React.Component{

  componentWillUpdate() {
    LayoutAnimation.spring();
  }

  renderDescription() {

    const { item, expand } = this.props;

    if (expand) {
      const {DescriptionStyle} = styles;
      return (
        <CardSection>
          <Text style={DescriptionStyle}>{item.description}</Text>
        </CardSection>
      )
    }
  }
  render() {

    const {TitleStyle} = styles;

    const {id, title} = this.props.item;

    return (
      <TouchableWithoutFeedback
        onPress={() => this.props.selectItem(id)}
      >
        <View>
          <CardSection>
            <Text style={TitleStyle}>{title}</Text>
          </CardSection>
          {this.renderDescription()}
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = {
  TitleStyle: {
    fontSize: 19,
    paddingLeft: 14
  },
  DescriptionStyle: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 13
  }
};

const mapStateToProps = (state, ownProps) => {

  const expand = state.selectedItem === ownProps.item.id;

  return {expand};
};

export default connect(mapStateToProps, actions)(ListItem);