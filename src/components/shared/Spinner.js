import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = ({ spinnerSize }) => {
  const { spinnerStyle } = styles;
  return (
    <View style={spinnerStyle}>
      <ActivityIndicator size={spinnerSize || 'large'}/>
    </View>
  )
};

const styles = {
  spinnerStyle: {
    flex: 1,
    alignSelf: 'center'
  }
};

export { Spinner };