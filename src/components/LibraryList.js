import React from 'react';
import { FlatList } from 'react-native';
import { connect } from 'react-redux';
import ListItem from './ListItem'

class LibraryList extends React.Component{

  renderItem({ item, index }) {
    return <ListItem item={item}/>
  }


  render() {
    return (
      <FlatList
        data={this.props.data}
        renderItem={this.renderItem}
        keyExtractor={(item, index) => item.title}
      />
    )
  }
}

const mapStateToProps = state => {
  return { data: state.libraries }
};

export default connect(mapStateToProps)(LibraryList);